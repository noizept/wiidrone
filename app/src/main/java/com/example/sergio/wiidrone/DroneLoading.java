package com.example.sergio.wiidrone;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class DroneLoading extends ActionBarActivity {

    private TextView _textIp;
    private int ip=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_drone_loading);
        _textIp=(TextView) findViewById(R.id.ip_servidor);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_H:
                Controls.ControlShow(this);
                break;
            case KeyEvent.KEYCODE_P :
             if(ip>=255) return true;
               ip++;
               break;
            case KeyEvent.KEYCODE_M:
                if(ip<=1) return true;
                ip--;
                break;
            case KeyEvent.KEYCODE_DPAD_CENTER:
                Intent dronePilot=new Intent(this,DronePilot.class);
                dronePilot.putExtra("IP_DO_SERVIDOR","192.168.1."+Integer.toString(ip));
                startActivity(dronePilot);
                return true;


        }
      _textIp.setText(Integer.toString(ip));
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drone_loading, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
