package com.example.sergio.wiidrone;


import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.sergio.wiidrone.mjpeg.MjpegInputStream;
import com.example.sergio.wiidrone.mjpeg.MjpegView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class DronePilot extends ActionBarActivity {

    //----------------DECLARAÇOES FILIPE
    Socket socket = null;
    DataOutputStream dataOutputStream = null;
    DataInputStream dataInputStream = null;
    public static String  serverIPAdress = "192.168.43.143";
    int serverPort = 3001;
    //---------------------------------------


    //---------------DECLARAÇOES VIDEO
    MjpegView videoView;
    String videoURL;
    //--------------------------------

    //-------------DECLARAÇOES IP DO SERVIDOR E VELOCIDADE
    private String mServidor;
    private double _velocidadeDrone=0.1;
    //-------------------------------

/*
    private void setSpeedStatusBar(){

        droneSpeed.setText(String.format("%.2f",_velocidadeDrone));
        droneSpeed.setTextColor(Color.RED);
*/




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drone_pilot);
        Bundle extras = getIntent().getExtras();
        mServidor= extras.getString("IP_DO_SERVIDOR");

        // MUDANÇA ENTRE SIMULADOR E DRONE
        serverIPAdress = mServidor;

        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    socket = new Socket(serverIPAdress, serverPort);
                    dataOutputStream = new DataOutputStream(socket.getOutputStream());
                    dataInputStream = new DataInputStream(socket.getInputStream());
                }catch(Exception e){
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();

        setVideo();
    }

    private void setVideo() {
        videoURL="http://"+serverIPAdress+":3002/nodecopter.mjpeg";
        MjpegInputStream mjp = new MjpegInputStream(null);
        mjp = mjp.read(videoURL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        videoView = new MjpegView(this);
        setContentView(videoView);
        videoView.setSource(mjp);
        videoView.setDisplayMode(MjpegView.SIZE_BEST_FIT);
        videoView.startPlayback();
        videoView.showFps(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drone_pilot, menu);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){

            //Comandos
            case KeyEvent.KEYCODE_H:
                Controls.ControlShow(this);
                break;
            //botão 1
            case KeyEvent.KEYCODE_1 :
                new CommandWorkerThread("[\"takeoff\",[],1]\n").start();
                break;
            //botaão 2
            case KeyEvent.KEYCODE_2:
                new CommandWorkerThread("[\"land\",[],1]\n").start();
                break;
            //Dpad up
            case KeyEvent.KEYCODE_DPAD_UP:
                new CommandWorkerThread("[\"up\",["+_velocidadeDrone+"],2]\n").start();
                break;
            //Dpad down
            case  KeyEvent.KEYCODE_DPAD_DOWN:
                new CommandWorkerThread("[\"down\",["+_velocidadeDrone+"],2]\n").start();
                break;
            //Botão A
            case  KeyEvent.KEYCODE_DPAD_CENTER:
                new CommandWorkerThread("[\"stop\",[],1]\n").start();
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                new CommandWorkerThread("[\"left\",["+_velocidadeDrone+"],2]\n").start();
                break;
            //Dpad Right
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                new CommandWorkerThread("[\"right\",["+_velocidadeDrone+"],2]\n").start();
                break;
            //Botão B
            case KeyEvent.FLAG_KEEP_TOUCH_MODE:
                new CommandWorkerThread("[\"front\",["+_velocidadeDrone+"],2]\n").start();
                break;
            //Botão Z
            case KeyEvent.KEYCODE_Z:
                new CommandWorkerThread("[\"back\",["+_velocidadeDrone+"],2]\n").start();
                break;
            //Botão C
            case KeyEvent.KEYCODE_C:
                new CommandWorkerThread("[\"calibrate\",[],1]\n").start();
                break;

            //Nunchuck
            case  KeyEvent.KEYCODE_A:
                new CommandWorkerThread("[\"counterClockwise\",["+_velocidadeDrone+"],2]\n").start();
                break;
            case KeyEvent.KEYCODE_D:
                new CommandWorkerThread("[\"clockwise\",["+_velocidadeDrone+"],2]\n").start();
                break;

            //Aumentar Velocidade
            case KeyEvent.KEYCODE_P:
                if(_velocidadeDrone<0.5)
                    _velocidadeDrone+=0.05;
                break;
            //reduzir velocidade
            case KeyEvent.KEYCODE_M:
                if(_velocidadeDrone>0.05)
                    _velocidadeDrone-=0.05;
                break;

        }

        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.FLAG_KEEP_TOUCH_MODE:
                new CommandWorkerThread("[\"front\",[0.0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_Z:
                new CommandWorkerThread("[\"back\",[0.0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                new CommandWorkerThread("[\"left\",[0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                new CommandWorkerThread("[\"right\",[0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_A:
                new CommandWorkerThread("[\"counterClockwise\",[0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_D:
                new CommandWorkerThread("[\"clockwise\",[0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
                new CommandWorkerThread("[\"up\",[0],2]\n").start();
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                new CommandWorkerThread("[\"down\",[0],2]\n").start();
                break;
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class CommandWorkerThread extends Thread{

        private String _command="";

        public CommandWorkerThread(String command){
            _command = command;
        }



        byte[] response = new byte[256];
        @Override
        public void run(){
            try {
                dataOutputStream.writeBytes(_command);
                dataInputStream.readFully(response);
                dataOutputStream.flush();

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        if (dataOutputStream != null) {
            try {
                dataOutputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        if (dataInputStream != null) {
            try {
                dataInputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}